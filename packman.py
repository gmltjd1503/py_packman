import random

print("<<<<<<<  >>>>>>>")
print("<<< Pack Man >>>")
print("<<<<<<<  >>>>>>>\n\n")
print("[WARNING] Ghost Come !!!\n")

# while문 버전
while True :
    print("> Choose Active <")
    print("> [1]. RUN <")
    print("> [2]. Use ITEM <")
    print("> [3]. FIGHT <")
    print("> [4]. EXIT <\n")

    choose = int(input("INPUT >> "))

    randomVar = random.randint(0, 100)
    #print("Check Rand Var >> ", randomVar)

    if choose == 1:
        randomVar /= 10
        if randomVar < 2:
            print("Character RUN Succeeded")
        else:
            print("Character DEAD.....")
    elif choose == 2:
        print("< Charater Using the ITEM >")
        print("> [1]. Use Potion <")
        print("> [2]. Use Pistol <")
        print("> [3]. Use Sword <")
        print("> [4]. Don't Use ITEM <")

    elif choose == 3:
        print("Character Dead")
    elif choose == 4:
        print("Exit...")
        break
    else:
        print("incorrect input. Re-input please.")


# For문 버전
"""
for index in range (10) :
    print("> Choose Active <")
    print("> [1]. RUN <")
    print("> [2]. Use ITEM <")
    print("> [3]. FIGHT <")
    print("> [4]. EXIT <\n")

    choose = int(input("INPUT >> "))

    #randomVar = random.randint(0, 100)
    #print("Check Rand Var >> ", randomVar)

    if choose == 1:
        randomVar /= 10
        if randomVar < 2:
            print("Character RUN")
        else:
            print("Character DEAD.....")
    elif choose == 2:
        print("Charater Using the ITEM")
    elif choose == 3:
        print("Character Dead")
    elif choose == 4:
        print("Exit...")
        break
    else:
        print("incorrect input. Re-input please.")
    index = 0
"""