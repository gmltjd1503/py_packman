
# 사용자 정의
from InterfaceManager import *
from GameManager import *

# 시스템
import System

System.Clear()

from msvcrt import getch

while True:
    UI_Manager.instance().PrintMainLogo()
    menuNumber = int(input())

    if menuNumber == 1:
        # 눈속임용 프로그레스 로딩바
        UI_Manager.instance().PrintLoadingBar()
        # 게임매니저 인스턴스를 통해 게임 스타트
        Game_Manager.instance().GameStart()
    elif menuNumber == 2:
        print("\n\t\t     프로그램을 종료합니다.")
        break
    else:
        print("\n\t\t다시 입력 해주시기 바랍니다.")
        System.Delay_N_Clear(2)