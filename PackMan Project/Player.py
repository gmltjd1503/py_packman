
import System

class PlayerInfo:
    # 정보 변수
    dict_PlayerInfo = {
        "PlayerName" : ""
    }

    # 생성자
    def __init__(self):
        self.dict_PlayerInfo["Lv"] = 1
        self.dict_PlayerInfo["AtkDmg"] = 20
        self.dict_PlayerInfo["ArmDfs"] = 3
        self.dict_PlayerInfo["CurHp"] = 100
        self.dict_PlayerInfo["MaxHp"] = 100
        self.dict_PlayerInfo["CurExp"] = 0
        self.dict_PlayerInfo["MaxExp"] = 1000
        self.dict_PlayerInfo["Gold"] = 5000
        self.dict_PlayerInfo["IsAlive"] = True


    def AcheiveExp(self, exp):
        self.dict_PlayerInfo["CurExp"] += exp

        if(self.dict_PlayerInfo["CurExp"] >= self.dict_PlayerInfo["MaxExp"]):
            tempExp = self.dict_PlayerInfo["CurExp"] - self.dict_PlayerInfo["MaxExp"]
            self.dict_PlayerInfo["CurExp"] = tempExp
            self.LevelUp()

    def LevelUp(self):
        self.dict_PlayerInfo["Lv"] += 1

        if self.dict_PlayerInfo["Lv"] < 10:
            self.dict_PlayerInfo["AtkDmg"] += 5
            self.dict_PlayerInfo["ArmDfs"] += 1
            self.dict_PlayerInfo["MaxHp"] += 20
            self.dict_PlayerInfo["CurHp"] = self.dict_PlayerInfo["MaxHp"]
            self.dict_PlayerInfo["MaxExp"] += 200
        elif self.dict_PlayerInfo["Lv"] >= 10 and self.dict_PlayerInfo["Lv"] < 20:
            self.dict_PlayerInfo["AtkDmg"] += int(self.dict_PlayerInfo["Lv"] / 2)
            self.dict_PlayerInfo["ArmDfs"] += 2
            self.dict_PlayerInfo["MaxHp"] += 10 + int(self.dict_PlayerInfo["Lv"] / 2)
            self.dict_PlayerInfo["CurHp"] = self.dict_PlayerInfo["MaxHp"]
            self.dict_PlayerInfo["MaxExp"] += 180 + (int(self.dict_PlayerInfo["Lv"] / 2) * 10)

        print("  [System] : 레벨업 하셨습니다. 현재 레벨 [{0}]" .format(self.dict_PlayerInfo["Lv"]))
        System.Delay(3)

    def AcheiveGold(self, gold):
        self.dict_PlayerInfo["Gold"] += gold

    def TakenDamage(self, dmg):
        self.dict_PlayerInfo["CurHp"] -= dmg

        if self.dict_PlayerInfo["CurHp"] <= 0:
            self.dict_PlayerInfo["CurHp"] = 0
            self.BehaviorDead()

    def BehaviorDead(self):
        self.dict_PlayerInfo["IsAlive"] = False

    def BehaviorAlive(self):
        self.SettingIsAlive(True)
        tempHp = (int)(self.dict_PlayerInfo["MaxHp"] / 2)
        self.SettingCurHp(tempHp)


    


    
    def SettingName(self, name):
        self.dict_PlayerInfo["PlayerName"] = name

    def GetName(self):
        if (not self.dict_PlayerInfo["PlayerName"]):
            return None
        else:
            return self.dict_PlayerInfo["PlayerName"]

    def SettingIsAlive(self, isAlive):
        self.dict_PlayerInfo["IsAlive"] = isAlive

    def SettingCurHp(self, hp):
        self.dict_PlayerInfo["CurHp"] = hp
    def SettingMaxHp(self, hp):
        self.dict_PlayerInfo["MaxHp"] = hp
    def SettingCurExp(self, exp):
        self.dict_PlayerInfo["CurExp"] = exp
    def SettingGold(self, gold):
        self.dict_PlayerInfo["Gold"] = gold

    def GetIsAlive(self):
        return self.dict_PlayerInfo["IsAlive"]
    
    def GetLv(self):
        return self.dict_PlayerInfo["Lv"]
    def GetCurHp(self):
        return self.dict_PlayerInfo["CurHp"]
    def GetMaxHp(self):
        return self.dict_PlayerInfo["MaxHp"]
    def GetMaxExp(self):
        return self.dict_PlayerInfo["MaxExp"]
    def GetCurExp(self):
        return self.dict_PlayerInfo["CurExp"]
    def GetAtkDmg(self):
        return self.dict_PlayerInfo["AtkDmg"]
    def GetArmDfs(self):
        return self.dict_PlayerInfo["ArmDfs"]
    def GetGold(self):
        return self.dict_PlayerInfo["Gold"]