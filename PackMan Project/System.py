import os
import time

def Delay_N_Clear(param_time):
    time.sleep(param_time)
    os.system("cls")

def Clear_N_Delay(param_time):
    os.system("cls")
    time.sleep(param_time)

def Clear():
    os.system("cls")

def Delay(param_time):
    time.sleep(param_time)