
import System
import Player

class UI_Manager:
    _instance = None

    @classmethod
    def _getInstance(cls):
        return cls._instance

    @classmethod
    def instance(cls, *args, **kwargs):
        cls._instance = cls(*args, **kwargs)
        cls.instance = cls._getInstance
        return cls._instance
    
    def PrintMainLogo(self):
        print("======================== W E L C @ M E ========================")
        print("\t\t     < Pack Man RPG Game >\n")
        print("\t\t\t  < M E N U >")
        print("\t\t\t 1. Game Start")
        print("\t\t\t 2. Exit\n")
        print("\t\t    Choose Menu Input  >>  ", end = '')

    def PrintLoadingBar(self):
        System.Clear()
        print("\n\t○ ○ ○ ○ ○ ○ ○ ○ ○ ○")
        print("\t L o a d i n g . ")
        System.Delay_N_Clear(0.35)
        print("\n\t● ○ ○ ○ ○ ○ ○ ○ ○ ○")
        print("\t L o a d i n g . .")
        System.Delay_N_Clear(0.35)
        print("\n\t● ● ○ ○ ○ ○ ○ ○ ○ ○")
        print("\t L o a d i n g . . .")
        System.Delay_N_Clear(0.35)
        print("\n\t● ● ● ○ ○ ○ ○ ○ ○ ○")
        print("\t L o a d i n g . .")
        System.Delay_N_Clear(0.35)
        print("\n\t● ● ● ● ○ ○ ○ ○ ○ ○")
        print("\t L o a d i n g .")
        System.Delay_N_Clear(0.35)
        print("\n\t● ● ● ● ● ○ ○ ○ ○ ○")
        print("\t L o a d i n g . .")
        System.Delay_N_Clear(0.35)
        print("\n\t● ● ● ● ● ● ○ ○ ○ ○")
        print("\t L o a d i n g . . .")
        System.Delay_N_Clear(0.35)
        print("\n\t● ● ● ● ● ● ● ○ ○ ○")
        print("\t L o a d i n g . .")
        System.Delay_N_Clear(0.35)
        print("\n\t● ● ● ● ● ● ● ● ○ ○")
        print("\t L o a d i n g . ")
        System.Delay_N_Clear(0.35)
        print("\n\t● ● ● ● ● ● ● ● ● ○")
        print("\t L o a d i n g . .")
        System.Delay_N_Clear(0.35)
        print("\n\t● ● ● ● ● ● ● ● ● ●")
        print("\t L o a d i n g . . .")
        System.Delay_N_Clear(0.1)

    def PrintProfile(self):
        print("===================")
        print("   Create Player")
        print("===================")
        print("□□□□□□□■■■■■□□□□□□□")
        print("□□□□□□■■■■■■■□□□□□□")
        print("□□□□□□■■■■■■■□□□□□□")
        print("□□■■■■■■■■■■■■■■■□□")
        print("□■■■■■■■■■■■■■■■■■□")
        print("□■■■■■■■■■■■■■■■■■□")

    def PrintUpdateTitle(self):
        print("=========================================================================")
        print("\t\t < <  P A C K    M A N    W @ R L D  > >")
        print("=========================================================================\n\n")

    def PrintUpdateCharacter(self):
        print("\t○○○○○○○○○○○○")
        print("\t○       ●  ○")
        print("\t○     ○○○○○○")
        print("\t○     ○○○○○○")
        print("\t○          ○")
        print("\t○○○○○○○○○○○○")
        print("-------------------------------------------------------------------------")

    def PrintUpdateMainMenu(self):
        print("    1. 모험하러 가기        2. 캐릭터 정보 보기        3. 게임 종료")
        print("\t\t\tI n p u t  >>  ", end = '')

    def PrintUpdateMain(self):
        self.PrintUpdateTitle()
        self.PrintUpdateCharacter()
        self.PrintUpdateMainMenu()

    def PrintHuntMainMenu(self):
        print("         이동하시려면 '→' 키를 눌러주세요.")
        print("         모험을 그만 두시려면 'ESC' 키를 눌러주세요.")

    def PrintHuntMain(self):
        System.Delay_N_Clear(0.3)
        self.PrintUpdateTitle()
        self.PrintUpdateCharacter()
        self.PrintHuntMainMenu()

    def PrintHuntMove(self):
        System.Delay_N_Clear(0.3)
        self.PrintUpdateTitle()
        print("\t  ○○○○○○○○○○○○")
        print("\t  ○       ●  ○")
        print("\t  ○     ○○○○○○")
        print("\t  ○     ○○○○○○")
        print("\t  ○          ○")
        print("\t  ○○○○○○○○○○○○")
        print("-------------------------------------------------------------------------")
        print("          이   동   중 .")
        System.Delay_N_Clear(0.3)

        self.PrintUpdateTitle()
        print("\t    ○○○○○○○○○○○○")
        print("\t    ○       ●  ○")
        print("\t    ○     ○○○○○○")
        print("\t    ○     ○○○○○○")
        print("\t    ○          ○")
        print("\t    ○○○○○○○○○○○○")
        print("-------------------------------------------------------------------------")
        print("          이   동   중 . .")
        System.Delay_N_Clear(0.3)

        self.PrintUpdateTitle()
        print("\t      ○○○○○○○○○○○○")
        print("\t      ○       ●  ○")
        print("\t      ○     ○○○○○○")
        print("\t      ○     ○○○○○○")
        print("\t      ○          ○")
        print("\t      ○○○○○○○○○○○○")
        print("-------------------------------------------------------------------------")
        print("          이   동   중 . . .")
        System.Delay_N_Clear(0.3)

        self.PrintUpdateTitle()
        print("\t    ○○○○○○○○○○○○")
        print("\t    ○       ●  ○")
        print("\t    ○     ○○○○○○")
        print("\t    ○     ○○○○○○")
        print("\t    ○          ○")
        print("\t    ○○○○○○○○○○○○")
        print("-------------------------------------------------------------------------")
        print("          이   동   중 . .")
        System.Delay_N_Clear(0.3)

        self.PrintUpdateTitle()
        print("\t  ○○○○○○○○○○○○")
        print("\t  ○       ●  ○")
        print("\t  ○     ○○○○○○")
        print("\t  ○     ○○○○○○")
        print("\t  ○          ○")
        print("\t  ○○○○○○○○○○○○")
        print("-------------------------------------------------------------------------")
        print("          이   동   중 .")
        System.Delay_N_Clear(0.3)

        self.PrintUpdateTitle()
        self.PrintUpdateCharacter()
    
    def PrintHuntGhostMenu(self):
        print("\n                 1. 공격 하기           2. 도망 가기")

    def PrintHuntGhost(self):
        System.Delay_N_Clear(0.2)
        self.PrintUpdateTitle()
        print("\t  ○○○○○○○○○○○○\t\t\t\t\t □□□□□□□")
        print("\t  ○       ●  ○\t\t\t\t\t□       □")
        print("\t  ○     ○○○○○○\t\t\t\t\t□  ■  ■ □")
        print("\t  ○     ○○○○○○\t\t\t\t\t□       □")
        print("\t  ○          ○\t\t\t\t\t□ □ □ □ □")
        print("\t  ○○○○○○○○○○○○\t\t\t\t\t□□ □ □ □□")
        print("-------------------------------------------------------------------------")

    def PrintHuntGhostAttack(self):
        System.Delay_N_Clear(0.2)
        self.PrintUpdateTitle()
        print("\t   ○○○○○○○○○○○○\t\t\t\t\t □□□□□□□")
        print("\t   ○       ●  ○\t\t\t\t\t□       □")
        print("\t   ○     ○○○○○○\t\t\t\t\t□  ■  ■ □")
        print("\t   ○     ○○○○○○\t\t\t\t\t□       □")
        print("\t   ○          ○\t\t\t\t\t□ □ □ □ □")
        print("\t   ○○○○○○○○○○○○\t\t\t\t\t□□ □ □ □□")
        print("-------------------------------------------------------------------------")

        System.Delay_N_Clear(0.2)
        self.PrintUpdateTitle()
        print("\t    ○○○○○○○○○○○○\t\t\t\t □□□□□□□")
        print("\t    ○       ●  ○\t\t\t\t□       □")
        print("\t    ○     ○○○○○○\t\t\t\t□  ■  ■ □")
        print("\t    ○     ○○○○○○\t\t\t\t□       □")
        print("\t    ○          ○\t\t\t\t□ □ □ □ □")
        print("\t    ○○○○○○○○○○○○\t\t\t\t□□ □ □ □□")
        print("-------------------------------------------------------------------------")

        System.Delay_N_Clear(0.2)
        self.PrintUpdateTitle()
        print("\t     ○○○○○○○○○○○○\t\t\t\t  □□□□□□□")
        print("\t     ○       ●  ○\t\t\t\t □       □")
        print("\t     ○     ○○○○○○\t\t\t\t □  ■  ■ □")
        print("\t     ○     ○○○○○○\t\t\t\t □       □")
        print("\t     ○          ○\t\t\t\t □ □ □ □ □")
        print("\t     ○○○○○○○○○○○○\t\t\t\t □□ □ □ □□")
        print("-------------------------------------------------------------------------")

        System.Delay_N_Clear(0.2)
        self.PrintUpdateTitle()
        print("\t    ○○○○○○○○○○○○\t\t\t\t □□□□□□□")
        print("\t    ○       ●  ○\t\t\t\t□       □")
        print("\t    ○     ○○○○○○\t\t\t\t□  ■  ■ □")
        print("\t    ○     ○○○○○○\t\t\t\t□       □")
        print("\t    ○          ○\t\t\t\t□ □ □ □ □")
        print("\t    ○○○○○○○○○○○○\t\t\t\t□□ □ □ □□")
        print("-------------------------------------------------------------------------")

        System.Delay_N_Clear(0.2)
        self.PrintUpdateTitle()
        print("\t   ○○○○○○○○○○○○\t\t\t\t\t □□□□□□□")
        print("\t   ○       ●  ○\t\t\t\t\t□       □")
        print("\t   ○     ○○○○○○\t\t\t\t\t□  ■  ■ □")
        print("\t   ○     ○○○○○○\t\t\t\t\t□       □")
        print("\t   ○          ○\t\t\t\t\t□ □ □ □ □")
        print("\t   ○○○○○○○○○○○○\t\t\t\t\t□□ □ □ □□")
        print("-------------------------------------------------------------------------")

    def PrintHuntGhostToHit(self):
        System.Delay_N_Clear(0.35)
        self.PrintUpdateTitle()
        print("\t  ○○○○○○○○○○○○\t\t\t\t       □□□□□□□")
        print("\t  ○       ●  ○\t\t\t\t      □       □")
        print("\t  ○     ○○○○○○\t\t\t\t      □  ■  ■ □")
        print("\t  ○     ○○○○○○\t\t\t\t      □       □")
        print("\t  ○          ○\t\t\t\t      □ □ □ □ □")
        print("\t  ○○○○○○○○○○○○\t\t\t\t      □□ □ □ □□")
        print("-------------------------------------------------------------------------")

        System.Delay_N_Clear(0.35)
        self.PrintUpdateTitle()
        print("\t  ○○○○○○○○○○○○\t\t\t\t     □□□□□□□")
        print("\t  ○       ●  ○\t\t\t\t    □       □")
        print("\t  ○     ○○○○○○\t\t\t\t    □  ■  ■ □")
        print("\t  ○     ○○○○○○\t\t\t\t    □       □")
        print("\t  ○          ○\t\t\t\t    □ □ □ □ □")
        print("\t  ○○○○○○○○○○○○\t\t\t\t    □□ □ □ □□")
        print("-------------------------------------------------------------------------")

        System.Delay_N_Clear(0.35)
        self.PrintUpdateTitle()
        print("\t  ○○○○○○○○○○○○\t\t\t\t       □□□□□□□")
        print("\t  ○       ●  ○\t\t\t\t      □       □")
        print("\t  ○     ○○○○○○\t\t\t\t      □  ■  ■ □")
        print("\t  ○     ○○○○○○\t\t\t\t      □       □")
        print("\t  ○          ○\t\t\t\t      □ □ □ □ □")
        print("\t  ○○○○○○○○○○○○\t\t\t\t      □□ □ □ □□")
        print("-------------------------------------------------------------------------")