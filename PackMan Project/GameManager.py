
from Player import *
from InterfaceManager import *
import System

# System
from msvcrt import getch
import random

class Game_Manager:
    _instance = None
    _player = None
    _enemyPool = []

    @classmethod
    def _getInstance(cls):
        return cls._instance

    @classmethod
    def instance(cls, *args, **kwargs):
        cls._instance = cls(*args, **kwargs)
        cls.instance = cls._getInstance
        return cls._instance

    def CreateEnemy(self):
        hp = random.randint(60, 100)
        atk = random.randint(6, 24)
        dfs = random.randint(0, 4)
        rewardExp = random.randint(180, 220)
        rewardGold = random.randint(320, 400)

        listEnemy = [hp, atk, dfs, rewardExp, rewardGold]
        return listEnemy

    def CreateEnemyPool(self):
        for i in range(5):
            self._enemyPool.append(self.CreateEnemy())

    # Debug용
    def PrintEnemyPool(self):
        for i in range(len(self._enemyPool)):
            print(self._enemyPool[i])
    
    def GameStart(self):
        self.CreateEnemyPool()
        #self.PrintEnemyPool()
        self.CreatePlayer()

    def CreatePlayer(self):
        while True:
            System.Delay_N_Clear(1)
            UI_Manager.instance().PrintProfile()
            
            name = input("\n이름을 입력하세요 >> ")
            print("\n'{0}' 해당하는 이름이 맞으십니까? (Y/N) >> " .format(name), end = '')
            choose = input()

            if choose == "y" or choose == "Y":
                self._player = PlayerInfo()
                self._player.SettingName(name)

                System.Delay(1)
                print("'{0}'님의 캐릭터가 생성되었습니다." .format(name))
                # Update 함수로 넘어간다.
                self.GameUpdate()

            elif choose == "n" or choose == "N":
                print("\n다시 입력 해주세요.")
                System.Delay(1)
            else:
                print("\n올바른 입력을 해주세요. (Y/N)")
                System.Delay(1)

    def GameUpdate(self):
        System.Delay_N_Clear(2)
        while True:
            UI_Manager.instance().PrintUpdateMain()
            choose = int(input())

            if choose == 1:
                self.HuntStart()
            elif choose == 2:
                self.ShowPlayerInfo()
            elif choose == 3:
                print("\n팩맨 게임을 이용해 주셔서 감사합니다. 3초 뒤 프로그램을 종료합니다.")
                System.Delay(3)
                exit()
            else:
                print("\t\t올바른 번호를 입력해주시기 바랍니다.")
                System.Delay_N_Clear(1.5)

    def ShowPlayerInfo(self):
        System.Delay_N_Clear(0.5)
        UI_Manager.instance().PrintUpdateTitle()
        print("\t\t\t  < 주 요 정 보>")
        print("\t    플 레 이 어 의   이 름         [ {0} ]" .format(self._player.GetName()))
        print("\t    플 레 이 어 의   레 벨         [ {0} ]" .format(self._player.GetLv()))
        print("---------------------------------------------------------------------")
        print("\n\t\t    체 력\t>>       [{0}/{1}]" .format(self._player.GetCurHp(), self._player.GetMaxHp()))
        print("\t\t  경 험 치\t>>       [{0}/{1}]" .format(self._player.GetCurExp(), self._player.GetMaxExp()))
        print("\t\t  공 격 력\t>>       Avg [{0}]" .format(self._player.GetAtkDmg()))
        print("\t\t  방 어 력\t>>       Abs [{0}]" .format(self._player.GetArmDfs()))
        print("\t\t  소유 <금>\t>>       [{0}]" .format(self._player.GetGold()))

        print("\n          [System] : 계속하려면 스페이스바를 누르세요.")
        key = ord(getch())

        while key != 32:
            key = ord(getch())
        
        System.Delay_N_Clear(0.2)


    def HuntStart(self):
        while True:
            UI_Manager.instance().PrintHuntMain()
            key = ord(getch())

            if key == 27: # ESC
                System.Delay_N_Clear(0.3)
                break
            elif key == 77: # 오른쪽 방향키
                UI_Manager.instance().PrintHuntMove()
                randNum = random.randint(0, 1000)
                randNum = int(randNum / 10)

                if randNum <= 35:
                    print("  [System] : 아무 일도 일어나지 않았습니다.")
                    print("  [System] : 계속 진행하려면 아무키나 눌러주세요.")
                    key2 = ord(getch())
                elif randNum > 35 and randNum <= 75:
                    System.Delay(1)
                    print("  [System] : 유령과 맞닥 뜨렸습니다.")
                    System.Delay(1.5)
                    self.Hunt()
                else:
                    self._player.SettingCurHp(self._player.GetMaxHp())
                    print("  [System] : 포션을 발견 하여 체력을 전부 회복했습니다.")
                    print("  [System] : 계속 진행하려면 아무키나 눌러주세요.")
                    key2 = ord(getch())

    def SystemLayoutPlayer(self):
        name = self._player.GetName()
        cHp = self._player.GetCurHp()
        mHp = self._player.GetMaxHp()
        cExp = self._player.GetCurExp()
        mExp = self._player.GetMaxExp()
        dmg = self._player.GetAtkDmg()
        dfs = self._player.GetArmDfs()

        print("  [System Layout]")
        print("  < '{0}' INFO >  HP [{1}/{2}]   EXP [{3}/{4}]  |  DMG [{5}]  DFS [{6}]" .format(name, cHp, mHp, cExp, mExp, dmg, dfs))

    def SystemLayoutEnemy(self, enemy):
        chp = enemy[0]
        atk = enemy[1]
        dfs = enemy[2]
        mhp = enemy[5]
        print("  < ENEMY >   HP [{0}/{1}]  DMG [{2}]  DFS [{3}]" .format(chp, mhp, atk, dfs))
    
    def Hunt(self):
        length = len(self._enemyPool) - 1
        # 오브젝트 풀에 들어있는 적 하나의 인덱스를 추출
        index = random.randint(0, length)
        # 리스트와 같은 객체는 전부 Call-By-Reference로 취급
        # 요소를 일일이 빼내어 복사만해서 사용하는 형태로 풀링 유지
        copyEnemy = []
        for i in range(len(self._enemyPool[index])):
            copyEnemy.append(self._enemyPool[index][i])
        copyEnemy.append(copyEnemy[0])

        isTurn = True
        enemyIsDead = False

        while self._player.GetCurHp() > 0 and copyEnemy[0] > 0:
            UI_Manager.instance().PrintHuntGhost()
            self.SystemLayoutPlayer()
            self.SystemLayoutEnemy(copyEnemy)
            
            if isTurn:  # Player Turn
                UI_Manager.instance().PrintHuntGhostMenu()
                key = ord(getch())
                if key == 49:   # 숫자 1 (공격)
                    UI_Manager.instance().PrintHuntGhostAttack()
                    atkDmg = random.randint(self._player.GetAtkDmg() - 5, self._player.GetAtkDmg() + 5)
                    copyEnemy[0] -= (atkDmg - copyEnemy[2])
                    if copyEnemy[0] <= 0:  # 인터페이스 표기용
                        copyEnemy[0] = 0
                    self.SystemLayoutPlayer()
                    self.SystemLayoutEnemy(copyEnemy)
                    print("\n  [System] : '{0}' 만큼 유령에게 데미지를 주었습니다." .format(atkDmg - copyEnemy[2]))
                    print("  [System] : 계속 진행하려면 스페이스바를 눌러주세요.")
                    anyKey = ord(getch())

                    while anyKey != 32:
                        anyKey = ord(getch())

                    if copyEnemy[0] <= 0:  # 몹을 잡았을 때
                        self._player.AcheiveExp(copyEnemy[3])
                        self._player.AcheiveGold(copyEnemy[4])
                        enemyIsDead = True

                    isTurn = False

                elif key == 50: # 숫자 2 (도망)
                    runNumber = random.randint(1, 100)
                    if runNumber < 33:
                        print("\n  [System] : 도망에 성공하셨습니다.")
                        System.Delay(2.5)
                        break
                    else:
                        print("\n  [System] : 도망에 실패하셨습니다. 턴이 넘어갑니다.")
                        System.Delay(2.5)
                        isTurn = False
            else:       # Enemy Turn
                UI_Manager.instance().PrintHuntGhostToHit()
                takenDmg = random.randint(copyEnemy[1] - 2, copyEnemy[1] + 4)
                finalDmg = takenDmg - self._player.GetArmDfs()
                self._player.TakenDamage(finalDmg)

                self.SystemLayoutPlayer()
                self.SystemLayoutEnemy(copyEnemy)

                print("\n  [System] : '{0}' 만큼 데미지를 입었습니다." .format(finalDmg))
                print("  [System] : 계속 진행하려면 스페이스바를 눌러주세요.")
                anyKey = ord(getch())

                while anyKey != 32:
                    anyKey = ord(getch())

                isTurn = True
        
        if enemyIsDead and self._player.GetIsAlive():
            print("\n  [System] : 유령을 잡는데 성공하셨습니다.")
            print("  [System] : 획득 [ {0} exp ]  [ {1} gold ]" .format(copyEnemy[3], copyEnemy[4]))
            print("  [System] : 계속 진행하려면 스페이스바를 눌러주세요.")

            anyKey = ord(getch())

            while anyKey != 32:
                anyKey = ord(getch())
        else:
            printGold = int(copyEnemy[4] / 2)
            tempGold = self._player.GetGold() - printGold
            self._player.SettingGold(tempGold)
            self._player.BehaviorAlive()

            print("\n  [System] : 유령과 싸우다 팩맨이 사망하였습니다.")
            print("  [System] : 손실 [ {0} gold ]  남은 소유 금 [ {1} gold ]" .format(printGold, self._player.GetGold()))
            print("  [System] : 계속 진행하려면 스페이스바를 눌러주세요.")

            anyKey = ord(getch())

            while anyKey != 32:
                anyKey = ord(getch())

