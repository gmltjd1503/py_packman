
list_A = [273, 32, 103, "문자열", True, False]

# 타입(자료형) 확인하기
# 출력 하면 <class 'list'> 라고 나온다.
print(type(list_A))
# 리스트 전체 출력
print(list_A)

# 리스트 요소의 접근하기
# 일반적인 컴퓨터언어들과 같이 '요소(인덱스)'값은 0부터 시작한다.
print("첫 요소의 값: ", list_A[0])
print("문자열 요소의 값: ", list_A[3])
print("문자열 요소 값 중 하나의 문자: ", list_A[3][0])

# 중첩 리스트 (2차원 배열과 다른 개념이다.)
list_B = [1, 2, 3, [100, 200, 300]]
# 각 요소를 차례대로 표기해보면 다음과 같다.
# 1 = list_B[0]
# 2 = list_B[1]
# 3 = list_B[2]
# 100 = list_B[3][0]
# 200 = list_B[3][1]
# 300 = list_B[3][2]

print(list_B)
print("list_B의 4번 째 위치의 2번 째 위치 값: ", list_B[3][1])

# 인덱스4는 범위를 벗어났다는 에러가 발생한다.
# 에러 내용 >> IndexError: list index out of range
# print(list_B[4])



# 리스트에 마지막 요소만을 출력하고 싶다면
# -1을 인덱스로 사용하면 된다.
print("마지막 요소 >> ", list_B[-1])

# 리스트의 길이를 구하는 함수 'len()'
print("리스트의 길이 >> ", len(list_B))

# 리스트 슬라이싱 ? ~ ?-1번 째까지 출력해라
# 0:4라고 한다면 0 ~ 3인덱스까지 출력을 한다.
print(list_A[0:4])
# ? 부터 끝 까지 다 출력하고 싶다면? 뒤 범위 값을 생략하면 된다.
print(list_A[1:])
# 처음부터 ? 까지 다 출력하면 앞 범위 값을 생략
print(list_A[:3])








###### 리스트 요소 추가하기
# append, insert
    # append는 vector에 push_back()과 같은 기능을 한다.
    # append는 리스트 요소 맨 뒤에 추가한다.

    # insert는 리스트 특정 구간에 어디든 삽입할 수 있다.

# extend()
    # 원래 리스트 뒤에 새로운 리스트의 요소 모두 추가를 할 때
    # 매개변수로 리스트를 입력한다.
    
    # >> list_A = [1, 2, 3];
    # >> list_B = [4, 5, 6];
    # >> list_A.extend(list_B) 또는 list_A.extend([4, 5, 6])


###### 리스트 요소 제거하기
# del
    # del 리스트명[인덱스]
    # 해당하는 인덱스의 위치에 요소를 제거하는 키워드

# pop()
    # 리스트명.pop(인덱스)
    # 해당하는 인덱스의 위치에 요소를 제거하는 함수

list_C = [0, 1, 2, 3, 4, 5]
print(list_C)
del list_C[1]
print(list_C)

# pop은 원래 Stack 자료구조에서 사용하는 것과 비슷하게 동작한다.
# 즉, 삭제를 하돼, 해당 요소 값을 반환을 함
number = list_C.pop(1)
print(list_C, "제거된 요소 값 >> ", number)

list_C.append(6)
print(list_C)



###### 리스트 내부에 있는지 확인하기
# in / not in 연산자
    # in - 리스트안에 해당 하는 값이 있는지 확인
    # not in - 리스트안에 해당 하는 값이 없는지 확인

print(0 in list_C)      # 0이 존재하기 때문에 True 반환
print(1 in list_C)      # 1이 존재하지 않기 때문에 False 반환



# min(), max(), sum() 함수
# min - 리스트 내부에서 최솟값을 구함
# max - 리스트 내부에서 최대값을 구함
# sum - 리스트 내부에 값을 모두 더함

# 실행 결과 값
print(min(list_C))      # 0
print(max(list_C))      # 6
print(sum(list_C))      # 18