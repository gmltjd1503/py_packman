
##### 반복문

# for

# 매개변수에 숫자를 한 개 넣는 방법
# for variable in range(n)
# variable은 0에서 n-1까지 출력한다.

for i in range(5):   # i가 0부터 4까지 총 5번 반복한다.
    print(i)

# 매개변수에 숫자를 두 개 넣는 방법
for i in range(1, 10):  # i가 1 ~ 9까지 총 9번 반복
    print(i)

# 매개변수에 숫자를 세 개 넣는 방법
# for variable range(i, j, k)
# i ~ j-1 까지 반복하며, variable 값이 k만큼 증감한다.
for i in range(0, 10, 2):
    print(i)

# range에 나누기 연산자를 사용도 가능하지만 나누기를 하면 캐스팅이 'float (실수형)'이 되기 때문에
# 사용자가 'int'로 캐스팅을 꼭 해주어야 한다.
for i in range(0, (int)(10 / 2)):
    print(i)






##### 반복문과 함께 리스트 사용하기

array = [273, 32, 10, 55]

for element in array:
    # 리스트에 요소들을 출력함
    print(element)

# 리스트의 길이를 통해 몇번 반복하는지 확인하기
for element in range(len(array)):
    print("{}번 째 반복: {}" .format(element+1, array[element]))

# 반대로 출력하기
for element in range(len(array)-1, -1, -1):
    print("{}번 째 반복: {}" .format((len(array) - element), array[element]))

# 반대로 반복하기 reversed()
for element in reversed(array):
    print(element)
for element in reversed(range(len(array))):
    print("{}번 째 반복: {}" .format((len(array) - element), array[element]))



##### 반복문과 함께 딕셔너리 사용하기

dictA = {
    "Name" : "건조된 망고",
    "Type" : "타입",
    "Ingredient" : ["망고", "설탕", "황산나트륨"],
    "Origin" : "필리핀"
}

print("[KEY : VALUE]")
for key in dictA:
    print("{0} : {1}" .format(key, dictA[key]))


# items() 함수를 사용하여 반복문과 딕셔너리 사용하기
# items() 함수는 키 뿐만이 아닌 값(Value) 까지 가져올 수 있다.
for key, element in dictA.items():
    print("dictA[{0}] = {1}" .format(key, element))








##### while

# while 조건식:
# 조건식이 계속 참(True)일 경우에는 break하기 전까지 반복한다

# [잠시 번외!!]
# Unix Time과 While 반복문을 사용한 Tick
# 해당 코드 테스트 시 주석(#) 삭제 후 실행
#import time

#number = 0

# 5초 동안 반복할 때
#target_tick = time.time() + 5
#while time.time() < target_tick:
#    number += 1

#print("5초 동안 {0} 번 반복했습니다." .format(number))




# break 키워드
# 반복문을 벗어날 때 사용하는 키워드로 while 말고도 for문에서도 사용할 수 있다.

i = 0

while True:
    print("{0} 번째 반복문입니다." .format(i))

    i = i + 1

    input_text = input("> 종료하시겠습니까? (Y) >> ")
    if input_text in ["y", "Y"]:
        print("반복을 종료합니다.")
        break


# continue 키워드
# 현재 반복을 생략하고 다음 반복으로 넘어간다.

array2 = [1, 2, 3, 4, 5]
for number in array2:
    if number < 3:
        continue

    print(number)