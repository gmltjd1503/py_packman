
# 1번 문제

# 1번 두 개의 숫자를 입력받아서 두 수를 비교하여 큰지 작은지 확인하기
num1 = int(input("1번 수 : "))
num2 = int(input("2번 수 : "))

# type을 통해 해당 변수의 '자료형'을 확인할 수 있다.
# 기존 input으로 입력받는 값은 str 형태이다.
print(type(num1))
print(type(num2))

print("당신이 입력한 수: ", num1, num2)
print("당신이 입력한 수: {0}, {1}" .format(num1, num2))
# 기본 자료형이 '문자열(%s)'이기 때문에 %d로하면 에러가 발생한다.
# 에러 내용 >> TypeError: %d format: a number is required, not str
print("당신이 입력한 수: %s, %s" % (num1, num2))

if num1 > num2:
    print("num1이 크다.")
elif num1 < num2:
    print("num2이 크다.")
else:
    print("두 수는 같다.")

# 수업 내용
# 기존 입력 받는 input함수는 '문자열'형태로 읽기 때문에 캐스팅(형변환)을 꼭 해주어야한다.
# num1 = int(input("1번 수: "))
# num2 = int(input("2번 수: "))

# 위와 같이 변경 했다면 숫자로 변경되었기 때문에 %d연산자를 사용해도 에러가 발생하지 않는다.