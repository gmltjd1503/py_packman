
# 딕셔너리 (Dictionary)

# 키를 기반으로 값을 저장하는 것을 말함
# C++을 공부했다면 에서 STL에서 제공하는 Map 또는 vector에다가 make_pair (Key-Value)로 이해하면 됨

# 중괄호 { }로 선언하며 '키 : 값' 형태를 콜론 : 으로 연결해서 만든다.

var_dictionary = {
    "KeyA" : 10,       # 문자열을 KEY 사용하기
    "KeyB" : 20,
    "KeyC" : 30,
    5:      40,        # 정수형으로 KEY 사용하기
    True:   50        # Bool로 KEY 사용하기
}

print(var_dictionary)




# 딕셔너리 요소의 접근하기
# 특정 '키(Key)' 값으로 접근할 수 있다.
print("KeyB의 값 : ", var_dictionary["KeyB"])




#### 딕셔너리에 값 추가하기/제거하기
# 딕셔너리[새로운 키] = 새로운 값
var_dictionary["KeyD"] = 100

# 이미 존재하고 있는 키일 경우 값이 대입된다.
var_dictionary["KeyA"] = 1
print(var_dictionary)


## 삭제 할 때
# del 딕셔너리명[키]
del var_dictionary["KeyB"]
print(var_dictionary)

# 만약 딕셔너리에 존재하지 않는 키에 접근할 경우 KeyError가 발생한다.
# 에러 내용 >> KeyError: 'Key'
# var_dictionary["Key"]




##### 내부에 키가 있는지 확인하기
# 똑같이 in을 사용하여 확인한다.
print("KeyA" in var_dictionary)     # True
print("KeyB" in var_dictionary)     # False



# get() 함수
# 딕셔너리의 키로 값을 추출
# 존재하지 않는 키에 접근할 경우 None 출력
# 리스트에 pop() 함수와 같은 역할